FROM python:3.10.4-alpine

WORKDIR /usr/aethred-lingua
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
EXPOSE 2000
ENV FLASK_APP=aethred-lingua.py
CMD ["sh", "-c", "flask run --host 0.0.0.0 --port 2000"]
